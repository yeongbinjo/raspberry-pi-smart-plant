#!/usr/bin/env python3
# coding: utf-8
import sys

import eel
from logging.handlers import TimedRotatingFileHandler
import logging
import os
import json
import traceback
import gevent
import Adafruit_DHT
import wiringpi
import smbus

__author__ = "PyLab"
__license__ = "MIT"

__version__ = "1.0.0"
__maintainer__ = "PyLab"
__email__ = "yeongbin.jo@pylab.co"
__status__ = "Production"


DEFAULT_CONFIG = {}
KEG_API_KEY = ''


logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] [%(levelname)s] [%(message)s]',
    handlers=[TimedRotatingFileHandler(filename='run.log', when='midnight', interval=1, encoding='utf-8')]
)


def log(message):
    logging.info(message)


@eel.expose
def save_config(config):
    """설정을 저장합니다"""
    with open('config.json', 'w') as f:
        f.write(json.dumps(config))


@eel.expose
def load_config():
    """설정을 로드합니다"""
    try:
        with open('config.json', 'r') as f:
            return json.loads(f.read())
    except (FileNotFoundError, ValueError):
        with open('config.json', 'w') as f:
            f.write(json.dumps(DEFAULT_CONFIG))
        return DEFAULT_CONFIG


@eel.expose
def get_data():
    humidity, temperature = Adafruit_DHT.read(Adafruit_DHT.DHT11, 4)
    valid = not(humidity is None and temperature is None)

    return {
        'valid': valid,
        'humidity': humidity,
        'temperature': temperature
    }


@eel.expose
def buzzer():
    pin = 20
    wiringpi.softToneCreate(pin)
    with open('mario.beep', 'r') as f:
        for line in f:
            if not line.strip():
                continue
            if line.find(':beep') == 0:
                _, freq, length = line.strip().split()
                wiringpi.softToneWrite(pin, int(freq.replace('frequency=', '')))
                wiringpi.delay(int(int(length.replace('length=', '').replace('ms;', '')) * 1.5))
            if line.find(':delay') == 0:
                wiringpi.softToneWrite(pin, 0)
                wiringpi.delay(int(int(line.replace(':delay ', '').replace('ms;', '')) * 0.6))
        wiringpi.softToneWrite(pin, 0)


@eel.expose
def alarm():
    pin = 20
    wiringpi.softToneCreate(pin)
    for _ in range(3):
        wiringpi.softToneWrite(pin, 1020)
        wiringpi.delay(150)
        wiringpi.softToneWrite(pin, 0)
        wiringpi.delay(50)


@eel.expose
def get_soil_humidity_data():
    pin = 21
    wiringpi.pinMode(pin, 0)
    return wiringpi.digitalRead(pin)


@eel.expose
def get_illuminance_data():
    I2C_CH = 1
    BH1750_DEV_ADDR = 0x23
    CONT_H_RES_MODE = 0x10
    i2c = smbus.SMBus(I2C_CH)
    lux_bytes = i2c.read_i2c_block_data(BH1750_DEV_ADDR, CONT_H_RES_MODE, 2)
    lux = int.from_bytes(lux_bytes, byteorder='big')
    i2c.close()
    return {
        'valid': True,
        'lux': lux
    }


if __name__ == '__main__':
    wiringpi.wiringPiSetupGpio()

    try:
        eel.init('front')
        eel.start('main.html', size=(720, 574), mode='chrome', port=58912)
    except:
        logging.info(traceback.format_exc())

    sys.exit(0)
